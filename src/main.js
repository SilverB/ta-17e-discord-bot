const Discord = require('discord.js');

const client = new Discord.Client();

const commands = require('./commands');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async (msg) => {
  console.log(`${msg.author.username}: ${msg.content}`);
  if (msg.content === 'ping') {
    msg.reply('Pong!');
  }
  if (msg.author.bot) {
    // ignore messages from bot's
    return;
  }
  Object.values(commands).forEach(async (command) => {
    const response = await command(msg.content);
    if (response) {
      msg.reply(response);
    }
  });
});

client.login(process.env.BOT_TOKEN);
