module.exports = async (msg) => {
  if (msg.indexOf('niiduk') === -1 && msg.trim() !== '!who') {
    return false;
  }
  return `Im niiduk (${process.env.CI_COMMIT_SHORT_SHA || 'xxx'}) and Hindrek Hannus is my creator! You can find my code at https://gitlab.com/hindrekhan/ta-17e-discord-bot`;
};
