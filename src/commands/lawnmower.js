var gis = require('g-i-s');

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low)
}

function getImgLink(err, res) {
    try {
        if (err) {
            console.log(err);
            return "Failed to get lawn mower";
        }
        
        var json = JSON.stringify(res, null, '  ');
    
        var ind = json.length;
    
        var imgNum = randomInt(0, ind);
        var imgLink = json[0][0].url

        return imgLink;
    }
    catch (e) {
        throw (e);
    }
}

module.exports = async(msg) => {
    if (msg.trim() !== '!showmower') {
        return false;
    }

    try {
        return gis('niiduk', getImgLink); 
    }
    catch (e) {
        console.error('e', e);
        return 'Error getting lawn mowers';
    }

    
}