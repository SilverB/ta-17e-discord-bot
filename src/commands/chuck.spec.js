const axios = require('axios');
const chuck = require('./chuck');

jest.mock('axios');

axios.get.mockResolvedValue({
  data: {
    value: {
      joke: 'Mock joke about Chuck Norris',
    },
  },
});

describe('chuck', () => {
  it('random string', async () => {
    const res = await chuck('hdfsh sdj dk sdjsdjf');
    expect(res).toBeFalsy();
  });
  it('!chuck', async () => {
    const res = await chuck('!chuck');
    expect(res).toBeTruthy();
    expect(res).toBe('Mock joke about Chuck Norris');
    expect(axios.get.mock.calls[0][0]).toBe('http://api.icndb.com/jokes/random');
  });
});
