const axios = require('axios');

module.exports = async (msg) => {
  if (msg.trim() !== '!chuck') {
    return false;
  }
  try {
    const { data } = await axios.get('http://api.icndb.com/jokes/random');
    return data.value.joke;
  } catch (e) {
    console.error('e', e);
    return 'error getting joke';
  }
};
