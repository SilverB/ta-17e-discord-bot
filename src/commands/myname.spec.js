const myname = require('./myname');

describe('myname', () => {
  it('no name given in msg', async () => {
    const res = await myname('hdfsh sdj dk sdjsdjf');
    expect(res).toBeFalsy();
  });
  it('!who', async () => {
    const res = await myname('!who');
    expect(res).toBeTruthy();
  });
  it('bot name given in message', async () => {
    const res = await myname('test niiduk dsfjsdfj');
    expect(res).toBeTruthy();
    expect(res).toMatch(/Hindrek Hannus/);
  });
});
