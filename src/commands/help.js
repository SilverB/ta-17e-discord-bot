module.exports = async (msg) => {
  if (msg.trim() !== '!help' && msg.trim().toLowerCase() !== '!help!niiduk') {
    return false;
  }
  return `I have following commands available:
 * !help / !niiduk!help - show help about commands
 * !who (or say my name) - share info about myself
 * !showmower - show random image of a lawn mower
  `;
};
