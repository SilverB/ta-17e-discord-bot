# TODO: use multi stage docker file
FROM node:10

WORKDIR /app
COPY . /app

RUN yarn install --production

CMD ["node", "src/main.js"]

